#!/bin/bash
# Mplayer Resume for the Nano Note.
# Resume playback of audio or video from where you left off/quit/stopped.
# Based on mplayerstart http://igurublog.wordpress.com/downloads/script-mplayerstart/
# Nano Note/Busybox/No X port.
# Requires: mplayer, dialog

# This script needs more work to get it completely working.
# Some sort of changelog is at the end of the script.

#COPYING ----------------------------------------------------------------------
# Copyright © 2013 Alexander .S.T. Ross
# Email=sourcecode1 (Anti Spam>@frypan} aross.me
# If that email address does not work incremnet the number by one eg; "sourcecode2" and keep on incermenting or vist http://contacting.aross.me
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

# === User Options ================================================================

# Location of resume files and temp files; approx 100MB free space required
data=""

# Optional: Parent folder(s) of your audio & video files for
#           searching for moved resumes
#           If you pass a .resume file to mplayerstart which contains
#           an invalid location for the media file (perhaps you moved it),
#           mplayerstart will recursively search these folders for the file
search1="/mnt/sd/vid/"
search2="/mnt/"

# Default mplayer video options
moptions="-noquiet -msglevel global=0:cplayer=4:vo=0:ao=0:demuxer=0:ds=0:demux=0:header=0:avsync=0:autoq=0:cfgparser=0:decaudio=0:decvideo=0:seek=0:open=0:dvd=0:parsees=0:lirc=0:stream=0:cache=0:xacodec=0:tv=0:osdep=0:spudec=0:playtree=0:input=0:vfilter=0:osd=0:network=0:cpudetect=0:codeccfg=0:sws=0:vobsub=0:subreader=0:osd-menu=0:afilter=0:netst=0:muxer=0:identify=0:ass=0:statusline=5:fixme=0"

mplayer=mplayer

# Audio Playback Options
guiplayer="$mplayer"

# Volume Restoration Options - see 'amixer' output to adjust if needed
# requires amixer (alsa-lib)
# primary amixer control and channel
master="Master"
masterchannel="Mono: Playback"
# secondary amixer control and channel
pcm="Master"
pcmchannel="Mono: Playback"

# === End User Options ==========================================================

help () {
	cat << EOF
mplayerstart-NN version 1.4.6
Creates resume file for resumable playback; disables screen blanking & restores last volume levels.

Usage: mplayerstart [OPTIONS] ~/video/movie1.ogv
       mplayerstart [OPTIONS] ~/.mplayerstart/movie1.ogv.resume
       mplayerstart --resumelast
       mplayerstart --resumepre
       mplayerstart --rewind
       mplayerstart --tvmode MODE
Options:
--resume            resumes playing file from last stop point
--resumelast        resumes playing last played media file
--resumepre         resumes playing previous-to-last played media file
--rewind            deletes last played resume file
--mopt "OPTIONS"    passes additional playback OPTIONS to mplayer
Note: Resume files are auto-deleted after 60 days.
Current data folder for resume files: $data
Default Mplayer options: $moptions

Based On:
http://igurublog.wordpress.com/downloads/script-mplayerstart/

EOF
	exit
}

saveroff () {
termblanktime=`cat /sys/module/kernel/parameters/consoleblank`
setterm -blank 0
}

saveron () {
setterm -blank $(($termblanktime/60))
}

dialogbox () {    #$1="text"  $2="red"OR"green"
if [ "$2" = "red" ]; then
	dialog --title "Mplayerstart" --colors --msgbox "\Zb\Z1$1\Zn" 10 70 5> /dev/null
else
	dialog --title "Mplayerstart" --msgbox "$1" 10 70 5> /dev/null
fi
}

# init
if [ ! -d "$data" ]; then
	data="$HOME/.config/mplayerstart"
fi
mkdir -p "$data/"
echo "$data/"
curdir=`pwd`

filename=""
restime=0
outfile=""
resume=0
resumeplay=""
seektime=""
rx=0
mopt=""

# parse command line
index=0
while [ "$1" != "" ];
do
	if [ "${1:0:1}" = "-" ]; then
		case "$1" in
			--help | -help )
				help
				;;
			--resume )
				resume=1
				;;
			--resumepre )
				resumepre=1
				;;
			--resumelast )
				resumelast=1
				;;
			--rewind )
				rewind=1
				;;
			--mopt )
				if [ "$mopt" != "" ]; then
					echo "Only one --mopt is permitted"
					exit 1
				fi
				if [ "$2" = "" ]; then
					echo Option $1 requires an argument
					exit 1
				fi
				mopt="$2"
				shift
				;;
			* )
				echo Unknown option $1
				exit 1
				;;
		esac
	else
		let "index+=1"
		case $index in
			1 )
				nowplaying="$1"
				;;
			* )
				echo Too many arguments
				exit 1
				;;
		esac
	fi
	shift
done

if (( resumepre + resumelast + rewind == 0 )) && [ "$nowplaying" = "" ]; then
	help
fi

# save volume
test=`amixer | grep "command not found"`
if [ "$test" != "" ] || [ "$master" = "" ] || [ "$pcm" = "" ]; then
	novol=1
else
	mastervol=`amixer sget "$master",0`
	mastervol=${mastervol##*$masterchannel }
	mastervol=${mastervol%% [*}
	pcmvol=`amixer sget "$pcm",0`
	pcmvol=${pcmvol##*$pcmchannel }
	pcmvol=${pcmvol%% [*}
fi

# resumes
if (( resumepre + resumelast + rewind != 0 )); then
	# Find most recently modified resume file
	cd "$data"
	lastresume=`ls -1t *.resume 2> /dev/null | head -n 1`
	cd "$curdir"
	if [ "$lastresume" = "" ]; then
		echo "No resume files found"
		dialogbox  "No resume files found" "red"
		exit 2
	fi
	if (( resumepre == 1 )); then
		touch --date="2000-01-01 00:00" "$data/$lastresume"
		cd "$data"
		lastresume=`ls -1t *.resume 2> /dev/null | head -n 1`
		cd "$curdir"
	fi
	if (( rewind == 1 )); then
		rm "$data/$lastresume"
		dialogbox "Rewind"
		lastresume=""
	fi
	if (( resumelast + resumepre != 0 )) && [ "$lastresume" != "" ]; then
		nowplaying="$data/$lastresume"
		resume=1
	fi
fi

# file to play?
if [ "$nowplaying" = "" ]; then
	exit
elif [ ! -e "$nowplaying" ]; then
	echo 'File not found: '"$nowplaying"
	dialogbox "File not found" "red"
	exit 2
fi
# resume file was passed on the command line?
if [ "${nowplaying##*.}" = "resume" ]; then
	resume=1
fi

# get resume info
if (( resume == 1 )); then
	# locate resume file
	if [ "${nowplaying##*.}" = "resume" ]; then
		# resume file was passed on the command line
		resumefile="$nowplaying"
	else
		resumefile="$data/`basename "$nowplaying"`.resume"
	fi
	# read resume file if present
	if [ -e "$resumefile" ]; then
		lcount=0
		seektime=""
		# Set the field seperator to a newline
		IFS="
		"
		for line in `cat "$resumefile"`;do
			case $lcount in
			0 ) seektime="$line";;
			1 ) resumeplay="$line";;
			2 ) mastervol="$line";;
			3 ) pcmvol="$line";;
			esac
			(( lcount +=1 ))
		done
		IFS=" "
		if [ "$seektime" != "" ]; then
			if [ "${nowplaying##*.}" = "resume" ]; then
				if [ "$resumeplay" = "" ]; then
					# accomodate prior versions of mplayerstart
					nowplaying=`basename "$nowplaying" ".resume"`
				else
					nowplaying="$resumeplay"
				fi
				if [ "$resumeplay" = "" ] || [ ! -e "$nowplaying" ]; then
					# find resumed file
					cleanname=`basename "$nowplaying"`
					# replace troublesome characters
					cleanname=${cleanname//"["/"\["}
					cleanname=${cleanname//"]"/"\]"}
					cleanname=${cleanname//"^"/"\^"}
					cleanname=${cleanname//"?"/"\?"}
					cleanname=${cleanname//"*"/"\*"}
					cleanname=${cleanname//"&"/"\&"}
					# search
					if [ "$search1" != "" ]; then
						if [ "$search2" == "" ]; then
							nowplaying=`find "$search1" -H -type f -name "$cleanname"`
						else
							nowplaying=`find "$search1" "$search2" -H -type f -name "$cleanname"`
						fi
						# extract the first file listed
						nowplaying=${nowplaying%%$'\n'*}
					else
						nowplaying=""
					fi
				fi
				if [ "$nowplaying" = "" ] || [ ! -e "$nowplaying" ]; then
					echo 'File not found'
					dialogbox "File not found" "red"
					exit 2
				fi
			fi
			echo "Resuming at $seektime seconds"
			seektime="-ss $seektime"
			# Resume volume levels
			if (( novol != 1 )); then
				amixer set "$master",0 $mastervol > /dev/null
				amixer set "$pcm",0 $pcmvol > /dev/null
			fi
		fi
	fi
fi

# set output filename
if [ ! -e "$data/resume-lock" ]; then
	filename=`basename "$nowplaying"`
	outfile="$data/$filename.$$.log"
fi

# CleanUp
# GNU Find has -execdir busybox dosn't.
# delete output files older than 6 days
find $data -follow -maxdepth 0 -type f -mtime +6 -name "*.log" -exec rm {} \;
# delete resume files older than 60 days
find $data -follow -maxdepth 0 -type f -mtime +60 -name "*.resume" -exec rm {} \;


# Audio or video?
if [ "`mplayer -vo null -ao null -frames 0 -quiet "$nowplaying" 2>/dev/null | grep 'Video: no video'`" ] ; then
	# play audio file
	video=0
	if [ "$outfile" = "" ]; then
		$guiplayer $seektime "$nowplaying"
	else
		$guiplayer $seektime "$nowplaying" > "$outfile"
	fi
else
	# play video file
	video=1
	#disable screensaver
	saveroff

	mp="$mplayer"
	mp2=""

	if [ "$outfile" = "" ]; then
		echo ">>> $mp $moptions $mopt $seektime \"$nowplaying\" $mp2"
		eval $mp $moptions $mopt $seektime \"$nowplaying\" $mp2
	else
		echo ">>> $mp $moptions $mopt $seektime \"$nowplaying\" $mp2 > \"$outfile\""
		eval $mp $moptions $mopt $seektime \"$nowplaying\" $mp2 > "$outfile"
	fi
fi

# process outfile - determine resume time
if [ "$outfile" != "" ] && [ -e "$outfile" ]; then
	mout=`tail -c 350 "$outfile"`
	if (( video == 1 )); then
		# did mplayer reach end of file?
		endoffile=${mout##*End of file)}	
		if [ "$endoffile" != "" ]; then
			restime=${mout##* V:}
			restime=${restime%% A-V:*}
			restime=${restime%%.*}
		fi
	else
		# calculate remaining audio seconds
		atime=${mout##*A:}
		atime=${atime%%.*}
		btime=${mout##*of}
		btime=${btime%%.*}
		(( remain = btime - atime ))
		if (( remain > 15 )); then
			restime=$atime
		fi
	fi
	rm -f "$outfile"
fi
echo "rx $rx $restime"
# save resume file
(( rx = restime - 5 ))
if (( rx > 10 )); then
	echo "$rx" > "$data/$filename.resume"
	echo "$nowplaying" >> "$data/$filename.resume"
	if (( novol != 1 )); then
		mastervol=`amixer sget "$master",0`
		mastervol=${mastervol##*$masterchannel }
		mastervol=${mastervol%% [*}
		pcmvol=`amixer sget "$pcm",0`
		pcmvol=${pcmvol##*$pcmchannel }
		pcmvol=${pcmvol%% [*}
	else
		mastervol=20
		pcmvol=20
	fi
	echo $mastervol >> "$data/$filename.resume"
	echo $pcmvol >> "$data/$filename.resume"
	echo "Resume file created: $rx seconds"
else
	rm -f "$data/$filename.resume"
fi

# cancel saver-suspend
if (( video == 1 )); then
	saveron
fi

exit

# Changelog
#No longer needs uuidgen.
#$XDG_CONFIG_HOME is where configs get saved into.
#Uses mplayer to know if file has any video.
